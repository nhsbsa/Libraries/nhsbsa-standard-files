# {project name}

## About the {project name}

TODO

## Quick start

### Building the application

TODO

### Running the application

TODO

## Contributions

We operate a [code of conduct](CODE_OF_CONDUCT.md) for all contributors.

See our [contributing guide](CONTRIBUTING.md) for guidance on how to contribute.

## License

Released under the [Apache 2 license](LICENCE.txt).
